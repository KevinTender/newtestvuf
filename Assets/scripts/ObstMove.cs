﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstMove : MonoBehaviour
{
    public Rigidbody2D objRig;
    public GameObject endPosition;
    public float speed;
    void Start()
    {
        objRig = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        objRig.AddForce((endPosition.transform.position - gameObject.transform.position).normalized * speed);
    }
}
