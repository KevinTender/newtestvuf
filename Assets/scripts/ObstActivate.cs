﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstActivate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnCollisionEnter2D(Collision2D other)
    {
        if(other.transform.tag =="guardian")
        {
            //gameObject.transform.SetParent(null);
            gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;

        }
    }
}
